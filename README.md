# Respeaker server

>This is an MQTT broker using mosca.

## Installation

```bash
npm install
```

## Run

```bash
npm start
```

## MQTT topics

Topic voor het gesproken commando vanuit the ReSpeaker Core v2.0

```bash
command/MAC-adres
```

Topic voor het aansturen van de ledjes van de ReSpeaker Core v2.0

```bash
ledjes/MAC-adres
```

Topic voor het antwoord van de robot op het commando.

```bash
zbos/dialog/text/command
```

## Meta

Arthur Verstraete - [ArthurVerstraete](https://github.com/ArthurVerstraete) - arthur.verstraete@student.vives.be

Frederik Feys – [FrederikFeys](https://github.com/FrederikFeys) – frederik.feys@student.vives.be
