const EventEmitter = require('events');

class Ticker extends EventEmitter {
    constructor() {
        super();
        this.seconds = 3;
        this.fired = 0;
    }

    getFired() {
        return this.fired;
    }

    setFired(number) {
        this.fired = number;
    }

    setupTimeout() {
        console.log('fire');
        this.fired = 1;
        setTimeout(() => {
            this.emit('timeout');
        }, this.seconds*1000);

    }

    emitImmediately() {
        console.log('fire');
        this.fired = 1;
        this.emit('timeout');
    }
}

module.exports = Ticker;