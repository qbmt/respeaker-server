// Mosca MQTT broker
const mosca = require('mosca');
const settings = { 
    http: {
        port: 1884,
        bundle: true,
        static: './'
    },
    port: 1883
};
const broker = new mosca.Server(settings);

class Broker {
    start() {
        broker.on('ready', ()=>{
            console.log('Broker is ready!');
        });
    
        broker.on('clientConnected', (client) => {
            console.log('#Broker# client connected:', client.id);
        });
    
        broker.on('clientDisconnected', function(client) {
            console.log('#Broker# client disconnected:', client.id);
        });
    
        broker.on('published', (packet) => {
            let message = packet.payload.toString();
            let topic = packet.topic.toString();
            console.log("#Broker# received message: " + message + " on topic: " + topic);
        });
    }
}

module.exports = Broker;