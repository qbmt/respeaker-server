// Mosca MQTT publisher
const mqtt = require('mqtt');
// var client = mqtt.connect('mqtt://localhost:1883'); // change address
let client;

class Publisher {
    constructor (addressOfTheBroker) {
        client = mqtt.connect(addressOfTheBroker);
    }
    sendMessage (topic, message){
        client.on('connect', ()=>{
            client.publish(topic, message);
            console.log('#Publisher# Message sent:' + message + " on: " + topic );
        });
    }
}

module.exports = Publisher;