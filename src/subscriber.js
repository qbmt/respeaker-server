const mqtt = require('mqtt');
const dotenv = require('dotenv');
dotenv.config();
const axios = require('axios');
const Publisher = require("./publisher");
const Ticker = require("./ticker");
let ticker = new Ticker();
const Connections = require("./connections");
const serverAddress = process.env.SERVER_ADDRESS_PREFIX + "://" + process.env.SERVER_ADDRESS + ":" + process.env.SERVER_ADDRESS_PORT;
let connections = new Connections(serverAddress);

let client;

let confidences = [];
let commands = [];
let topics = [];

class Subscriber {
    constructor(addressOfTheBroker) {
        client = mqtt.connect(addressOfTheBroker);
    }  
    
    subscribe(topic) {
        client.on('connect', ()=>{
            client.subscribe(topic);
        });
        
        client.on('message', (topic, message)=>{
            message = message.toString();
            topic = topic.toString();
            console.log("#Subscriber# " + message + " on topic: " + topic);

            let confidence = message.substring(0, message.indexOf(" "));
            let command = message.substring(message.indexOf(" ")+1, message.length);
            
            confidences.push(confidence.trim());
            commands.push(command.trim());
            topics.push(topic.trim());
            
            if (!Boolean(ticker.getFired())) {
                ticker.setupTimeout();
            }

            if (connections.getConnections() == confidences.length) {
                ticker.emitImmediately();
            }
        });
            
        ticker.on('timeout', (event) => {
            if (Boolean(ticker.getFired())) {
                console.log('timeout event');
                var max = 0;
                var maxIndex = 0;
                
                for (var i = 0; i < confidences.length; i++) {
                    if (confidences[i] > max) {
                        maxIndex = i;
                        max = confidences[i];
                    }
                }
                
                const serverAddress = process.env.SERVER_ADDRESS_PREFIX + "://" + process.env.SERVER_ADDRESS + ":" + process.env.SERVER_ADDRESS_PORT;
                
                const robotAddress = process.env.ROBOT_ADDRESS_PREFIX + "://" + process.env.ROBOT_ADDRESS + ":" + process.env.ROBOT_ADDRESS_PORT;
                const robotPublisher = new Publisher(robotAddress);
                
                let mac = topics[maxIndex].substring(topics[maxIndex].indexOf("/")+1, topics[maxIndex].length);
                let understood;
                
                if (commands[maxIndex] != "") {
                    const publisher = new Publisher(serverAddress);
                    console.log("command with highest conf: " + confidences[maxIndex] + "\t" + commands[maxIndex]);
                    if (parseFloat(confidences[maxIndex]) > 0.50) {
                        publisher.sendMessage("leds/" + mac, "understood");
                        robotPublisher.sendMessage("zbos/dialog/text/command", commands[maxIndex]);
                        understood = true;
                    } else {
                        publisher.sendMessage("leds/" + mac, "mis_understood");
                        understood = false;
                    }
                    
                    axios.post('http://127.0.0.1:4000/api/messages?key=' + process.env.API_KEY, {
                        sender: 'respeaker',
                        message: commands[maxIndex],
                        confidence: confidences[maxIndex],
                        understood: understood
                    })
                    .then(function (response) {
                        console.log("send message to api");
                        console.log(response.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                } else {
                    const publisher = new Publisher(serverAddress);
                    publisher.sendMessage("leds/" + mac, "mis_understood");
                }

                
                confidences = [];
                commands = [];
                topics = [];
                connections.setConnections(0);
                ticker.setFired(0);
            }
        });
    }
}

module.exports = Subscriber;