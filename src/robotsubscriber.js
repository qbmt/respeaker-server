const mqtt = require('mqtt');
const dotenv = require('dotenv');
dotenv.config();
const axios = require('axios');
const Publisher = require("./publisher");
let client;


class RobotSubscriber {
    constructor(addressOfTheBroker) {
        client = mqtt.connect(addressOfTheBroker);
    }  
    
    subscribe(topic) {
        client.on('connect', ()=>{
            client.subscribe(topic);
        });
        
        client.on('message', (topic, message)=>{
            message = message.toString();
            topic = topic.toString();
            console.log("#Robotsubscriber# " + message + " on topic: " + topic);
            if (topic == "zbos/dialog/tts/start") {
                axios.post('http://127.0.0.1:4000/api/messages?key=' + process.env.API_KEY, {
                    sender: 'robot',
                    message: message
                })
                .then(function (response) {
                    console.log("send message to api");
                    console.log(response.data);
                })
                .catch(function (error) {
                    console.log(error);
                });
            } else if (topic == "zbos/event/dialog/listen/started") {
                const serverAddress = process.env.SERVER_ADDRESS_PREFIX + "://" + process.env.SERVER_ADDRESS + ":" + process.env.SERVER_ADDRESS_PORT;
                const publisher = new Publisher(serverAddress);
                publisher.sendMessage("respeaker/config", "startListening");
            }

        });
    }
}

module.exports = RobotSubscriber;
