const mqtt = require('mqtt');
let client;

class Connections {
    constructor(addressOfTheBroker) {
      this.connections = 0;
      client = mqtt.connect(addressOfTheBroker);
      client.on('connect', ()=>{
          client.subscribe('status');
      });

      client.on('message', (topic, message)=>{
          message = message.toString();
          if (message=='connected') {
            this.connections++;
          }
          console.log("#Subscriber# " + message);
      });
    }

    getConnections() {
      return this.connections;
    }

    setConnections(number) {
      this.connections = number;
    }
}

module.exports = Connections;