const dotenv = require('dotenv');
dotenv.config();
const Broker = require("./src/broker");
const broker = new Broker();

const Subscriber = require("./src/subscriber");
const RobotSubscriber = require("./src/robotsubscriber");

const serverAddress = process.env.SERVER_ADDRESS_PREFIX + "://" + process.env.SERVER_ADDRESS + ":" + process.env.SERVER_ADDRESS_PORT;
const subscriber = new Subscriber(serverAddress);

const robotAddress = process.env.ROBOT_ADDRESS_PREFIX + "://" + process.env.ROBOT_ADDRESS + ":" + process.env.ROBOT_ADDRESS_PORT;
const robotSubscriber = new RobotSubscriber(robotAddress);

broker.start();
subscriber.subscribe("command/#");
robotSubscriber.subscribe("zbos/#");
